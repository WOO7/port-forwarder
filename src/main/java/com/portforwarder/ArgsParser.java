package com.portforwarder;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ArgsParser {

    private final static Pattern MAPPING_PATTERN = Pattern.compile("^([\\w.]*):(\\d+)=([\\w.]+):(\\d+)$");

    public static Map<String, String> doParse(String... args) {
        Map<String, String> mappings = parseMappings(args);
        if (mappings == null || mappings.isEmpty()) {
            String mapArg = System.getProperty("map");
            if (mapArg == null || mapArg.isEmpty()) {
                throw new IllegalArgumentException(String.format("mapping %s has error !", mapArg));
            }
            args = mapArg.split(",");
            System.out.println("detected jvm map argument " + mapArg);
            mappings = parseMappings(args);
        } else {
            System.out.println("detected program run arguments " + Arrays.toString(args));
        }
        if (mappings == null || mappings.isEmpty()) {
            System.err.println("java -jar port-forwarder.jar 127.0.0.1:80=127.0.0.1:9999 127.0.0.1:81=127.0.0.1:9999");
            System.err.println("java -Dmap=127.0.0.1:80=127.0.0.1:9999,127.0.0.1:81=127.0.0.1:9999 -jar port-forwarder.jar");
            System.exit(-1);
        }
        return mappings;
    }

    private static Map<String, String> parseMappings(String[] args) {
        if (args == null || args.length == 0) {
            return Collections.EMPTY_MAP;
        }
        Map<String, String> mappings = new HashMap<>();
        for (String entry : args) {
            entry = entry.trim();
            Matcher m = MAPPING_PATTERN.matcher(entry);
            if (!m.find()) {
                System.err.println(String.format("mapping %s has error !", entry));
                continue;
            }
            String[] mapping = entry.split("=");
            mappings.put(mapping[0], mapping[1]);
        }
        return mappings;
    }

}
