package com.portforwarder.core;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.Map;

public class ForwardService {

    private ForwardMapping mappings;

    public ForwardService(Map<String, String> props) {
        this.mappings = new ForwardMapping(props);
    }

    public ForwardService(String mappingPath) {
        try {
            this.mappings = new ForwardMapping(mappingPath);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public ForwardService(InputStream is) {
        this.mappings = new ForwardMapping(is);
    }

    public void startup() {
        final Map<InetSocketAddress, InetSocketAddress> map = mappings.toMappings();
        if (map == null || map.isEmpty()) {
            System.err.println("The config has error !");
            return;
        }
        for (Map.Entry<InetSocketAddress, InetSocketAddress> entry : map.entrySet()) {
            final InetSocketAddress from = entry.getKey();
            final InetSocketAddress to = entry.getValue();
            final String ruleName = "from: " + from.getHostString() + ":" + from.getPort() + " to: " + to.getHostString() + ":" + to.getPort();
            System.out.println(ruleName);
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        new TcpForwarder(from, to, ruleName).call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.setName(ruleName);
            thread.start();
        }
    }

}
