package com.portforwarder.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class ForwardMapping {

    private Map<InetSocketAddress, InetSocketAddress> map;

    public ForwardMapping(InputStream input) {
        try {
            Properties props = new Properties();
            props.load(input);
            Map<String, String> values = new HashMap<>();
            values.putAll((Map) props);
            this.map = parseMapping(values);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ForwardMapping(Map<String, String> props) {
        this.map = parseMapping(props);
    }

    public ForwardMapping(String file) throws FileNotFoundException {
        this(new FileInputStream(file));
    }

    public Map<InetSocketAddress, InetSocketAddress> toMappings() {
        return this.map;
    }

    public Map<InetSocketAddress, InetSocketAddress> parseMapping(Map<String, String> props) {
        HashMap<InetSocketAddress, InetSocketAddress> mappings = new HashMap<InetSocketAddress, InetSocketAddress>();
        Set<?> keySet = props.keySet();
        for (Object key : keySet) {
            String strKey = (String) key;
            String srcIP = "0.0.0.0";
            int srcPort = 0;
            if (strKey.contains(":")) {
                srcIP = strKey.substring(0, strKey.indexOf(":"));
                try {
                    srcPort = Integer.parseInt(strKey.substring(strKey.indexOf(":") + 1, strKey.length()));
                } catch (Exception localException) {
                }
                if (srcPort != 0) {
                    String value = props.get(strKey);
                    String descIP = "0.0.0.0";
                    int descPort = 0;
                    if (value.contains(":")) {
                        descIP = value.substring(0, value.indexOf(":"));
                        try {
                            descPort = Integer.parseInt(value.substring(value.indexOf(":") + 1, value.length()));
                        } catch (Exception localException1) {
                        }
                        if (descPort != 0) {
                            InetSocketAddress keyAddress = new InetSocketAddress(srcIP, srcPort);
                            InetSocketAddress valueAddress = new InetSocketAddress(descIP, descPort);
                            mappings.put(keyAddress, valueAddress);
                        }
                    }
                }
            }
        }
        return mappings;
    }
}
