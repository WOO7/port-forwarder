/*
 * Fwd: the port forwarding app
 * Copyright (C) 2016  Elixsr Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.portforwarder.core;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.concurrent.Callable;

/**
 * Skeleton taken from:
 * http://cs.ecs.baylor.edu/~donahoo/practical/JavaSockets2/code/
 * UDPEchoServerSelector.java
 * <p>
 * Created by Niall McShane on 21/02/2016.
 */
public class UdpForwarder extends Forwarder implements Callable<Void> {

    private static final int BUFFER_SIZE = 100000;
    public static final String PROTOCOL_NAME = "UDP";

    public UdpForwarder(InetSocketAddress form, InetSocketAddress to, String ruleName) {
        super(PROTOCOL_NAME, form, to, ruleName);
    }

    public Void call() throws IOException, BindException {

        try {
            ByteBuffer readBuffer = ByteBuffer.allocate(BUFFER_SIZE);

            DatagramChannel inChannel = DatagramChannel.open();
            inChannel.configureBlocking(false);

            try {
                inChannel.socket().bind(this.from);
            } catch (SocketException e) {
                throw new BindException(String.format(BIND_FAILED_MESSAGE, from.getPort(), protocol, ruleName), e);
            }

            Selector selector = Selector.open();
            inChannel.register(selector, SelectionKey.OP_READ, new ClientRecord(to));

            while (true) { // Run forever, receiving and echoing datagrams
                if (Thread.currentThread().isInterrupted()) {
                    inChannel.socket().close();
                    break;
                }

                int count = selector.select();
                if (count > 0) {

                    // Get iterator on set of keys with I/O to process
                    Iterator<SelectionKey> keyIter = selector.selectedKeys().iterator();
                    while (keyIter.hasNext()) {
                        SelectionKey key = keyIter.next(); // Key is bit mask

                        // Client socket channel has pending data?
                        if (key.isReadable()) {
                            // Log.i(TAG, "Have Something to READ");
                            handleRead(key, readBuffer);
                        }

                        // Client socket channel is available for writing and
                        // key is valid (i.e., channel not closed).
                        if (key.isValid() && key.isWritable()) {
                            handleWrite(key);
                        }
                        keyIter.remove();
                    }
                }
            }
        } catch (IOException e) {
            throw e;
        }

        return null;
    }

    public static void handleRead(SelectionKey key, ByteBuffer readBuffer) throws IOException {

        DatagramChannel channel = (DatagramChannel) key.channel();
        ClientRecord clientRecord = (ClientRecord) key.attachment();

        // Ensure the buffer is empty
        readBuffer.clear();

        // Receive the data
        channel.receive(readBuffer);

        // Get read to wrte, then send
        readBuffer.flip();
        channel.send(readBuffer, clientRecord.toAddress);

        // If there is anything remaining in the buffer
        if (readBuffer.remaining() > 0) {
            clientRecord.writeBuffer.put(readBuffer);
            key.interestOps(SelectionKey.OP_WRITE);
        }
    }

    public static void handleWrite(SelectionKey key) throws IOException {
        DatagramChannel channel = (DatagramChannel) key.channel();
        ClientRecord clientRecord = (ClientRecord) key.attachment();
        clientRecord.writeBuffer.flip(); // Prepare buffer for sending
        if (clientRecord.writeBuffer.remaining() > 0) {
            clientRecord.writeBuffer.compact();
        } else {
            key.interestOps(SelectionKey.OP_READ);
            clientRecord.writeBuffer.clear();
        }
    }

    static class ClientRecord {
        public SocketAddress toAddress;
        public ByteBuffer writeBuffer = ByteBuffer.allocate(BUFFER_SIZE);

        public ClientRecord(SocketAddress toAddress) {
            this.toAddress = toAddress;
        }
    }

}
