package com.portforwarder;

import com.portforwarder.core.ForwardService;

import java.util.Map;

public class Main {

    public static void main(String... args) {
        Map<String, String> props = ArgsParser.doParse(args);

        ForwardService service = new ForwardService(props);

        service.startup();
    }

}
